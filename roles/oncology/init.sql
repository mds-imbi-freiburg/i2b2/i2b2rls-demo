CREATE EXTENSION IF NOT EXISTS pgtap;

-- Create observation_fact view if not exists
SELECT exists (select * FROM information_schema.views WHERE table_schema = :'cell_schema' AND table_name = 'v_observation_fact') as view_exists;
\gset

\echo View exists: :view_exists
\if :view_exists
  \echo View exists: v_observation_fact
\else
    CREATE OR REPLACE VIEW :"cell_schema".v_observation_fact AS
        SELECT encounter_num, patient_num, concept_cd, pd.provider_id, pd.provider_path, start_date, modifier_cd
        FROM :"cell_schema".observation_fact as obs
        JOIN :"cell_schema".provider_dimension as pd ON pd.provider_id = obs.provider_id
\endif

