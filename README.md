# i2b2rls Example Project

In this project we show the options and capabilities of the `i2b2rls` stack.

To initiate a project folder with all the templates, create an empty directory
and call the init command:

~~~bash
mkdir i2b2rls-demo && cd i2b2rls-demo
i2b2fire --conf=. --init=True
~~~


### General config
First, we start by editing `i2b2.yaml`.  
In the basic configuration we enable debugging and set the container name of 
our wildfly instance:

~~~yaml
log-level: DEBUG 
wildfly-container: i2b2-wildfly
~~~

__Note:__ A well commented [example](https://gitlab.com/uklfr/mds/i2b2/i2b2rls/-/blob/master/i2b2rls/config.default/i2b2.yaml) of a project configuration file can be found in the main repository of i2b2rls.

### Project defaults
In the default project we define LDAP settings that are valid for all the projects:

~~~yaml
ldap:
  authentication-method: LDAP
  connection-url: ldap.harvard.example.org
  distinguished-name: uid=
  search-base: ou=people,ou=harvard,dc=example,dc=org
  security-authentication: simple
~~~

### SQL Templates
Next we modify the template sql scripts that will be the base for 
other project's sql scripts.
These are located at `i2b2rls-demo/roles/template/`.



### Project Oncology
The oncology project inherits all settings from the default project.  

For the Data Repository Cell (CRC) we set a Datasource user (ds-user) and Password.  
The corresponding sql scripts we create later will be used to create the user 
and the RLS policies in the Postgres Database.

To generate individual sql scripts for the oncology role use:
~~~bash
i2b2fire project generate_sql oncology
~~~

Our SQL scripts for the RLS policy will reference the value of `pg-vars > provider_filter`.   
The policy will return only rows from tables whose encounters/patients belong to the provider id listed:   
* LCS-I2B2:D000109099 (\i2b2\Providers\Oncology\Tak, John, MD\)
* LCS-I2B2:D000109079 (\i2b2\Providers\Oncology\Wilson, James, MD\)


This copies the template folder to a folder named after the Postgres role in `ds-role`.
~~~bash
roles/oncology_rls
~~~

Now in [roles/oncology_rls/main.sql](roles/oncology_rls/main.sql) we added a `psql include statement` that refers to the file `oncology_specific_things.sql`.  
This way SQL and RLS rules can be composed from multiple locations and build up hierarchically.

### Project Primary Care
In this project `name`, `description` and `project_path` are set explicitly:

* `name`: is displayed in the selection of projects when a user logs in to the frontend.  
* `project_path`: is used in the backend to identify the project in the i2b2 Cells (db_lookups).
* `description`: 

If these keys are ommited they are generated from the project name (primary_care).

`default-project-roles` is overridden from the default project with `USER` and `DATA_OBFSC`.   
These are the minimal required roles a user needs to login and query patients.

They key `sql-dir` is set to `template`.   
This tells the application that the Postgres role and RLS rules are to be created by the `template` role, whose files are in `i2b2rls-demo/roles/template`.
In consequence, there is no own folder for the project in `i2b2rls-demo/roles/`.

Creating roles like this makes sense if your subdepartments all need the same permissions and the only difference is the observation_facts they have access to.  
In that case you could use the same sql template for all while passing a different (set of) `provider_filter` for each.

### Project Genetics
Also the genetics project inherits all the settings from the default project.  
We override the `search-base` of the  ldap configuration, as every department
may have its own Organisational Unit in the LDAP tree.


#### Members
The previously defined member __jameswilson__ is referenced.
As it already exists it is not created, the values are not updated.

The member **james_parker** omits a password but has `ldap` set to an empty key.  
This indicates he is a ldap user. That means he won't need a password but the LDAP settings of his project set in `pm_user_params`.
